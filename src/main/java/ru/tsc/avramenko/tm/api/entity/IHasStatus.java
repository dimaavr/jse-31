package ru.tsc.avramenko.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import ru.tsc.avramenko.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
