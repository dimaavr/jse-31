package ru.tsc.avramenko.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.command.AbstractProjectCommand;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-change-status-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Change project status by index.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusValue);
        @NotNull final String userId = serviceLocator.getAuthService().getCurrentUserId();
        @Nullable final Project project = serviceLocator.getProjectService().changeStatusByIndex(userId, index, status);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectService().changeStatusByIndex(userId, index, status);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}